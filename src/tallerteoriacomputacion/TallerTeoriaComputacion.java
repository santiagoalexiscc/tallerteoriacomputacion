package tallerteoriacomputacion;

import java.util.Scanner;

public class TallerTeoriaComputacion {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Escriba una cadena");
        String u = sc.next();
        System.out.println("Escriba una segunda cadena");
        String v = sc.next();
        System.out.println("¿Que operacion quieres realizar a la cadena?"
                + "1: Concatenacion"
                + "2: Longitud de la cadena"
                + "3: Reflexion de la cadena (Invertir cadena)"
                + "4: Potencia de la cadena"
                + "5: Agregar un prefijo"
                + "6: Agregar un sufijo");
        int opcion = sc.nextInt();
        if (opcion == 1) {
            concatenacion(u, v);
        }
        if (opcion == 2) {
            System.out.println("La longitud de la primera cadena es: " + longitudCadena(u));
            System.out.println("La longitud de la primera cadena es: " + longitudCadena(v));
        }
        if (opcion == 3) {
            System.out.println(reflexionCadena(u));
            System.out.println(reflexionCadena(v));
        }
        if (opcion == 4) {
            System.out.println("Escriba el exponente de la cadena");
            int n = sc.nextInt();
            System.out.println(potencia(u, n));
            System.out.println(potencia(v, n));
        }
        if (opcion == 5) {
            System.out.println("¿Cuantos prefijos quieres agregar?");
            int x = sc.nextInt();
            System.out.println("Escriba " + x + " prefijo(s)");

            for (int i = 0; i < x; i++) {
                String s = sc.next();
                añadirPrefijo(u, s);
                añadirPrefijo(v, s);
            }
        }
        if (opcion == 6) {
            System.out.println("¿Cuantos sufijos quieres agregar?");
            int x = sc.nextInt();
            System.out.println("Escriba " + x + "sufijo(s)");

            for (int i = 0; i < x; i++) {
                String s = sc.next();
                añadirSufijo(u, s);
                añadirSufijo(v, s);
            }
        }
    }

    public static void concatenacion(String u, String v) {
        System.out.println(u + v);
        return;
    }

    public static String potencia(String u, int n) {
        String potenciado = "";
        if (n == 0) {
            System.out.print("La cadena es vacia");
        }
        for (int i = 1; i <= n; i++) {
            potenciado += u;
        }
        return potenciado;
    }

    public static int longitudCadena(String u) {
        return u.length();
    }

    public static String reflexionCadena(String u) {
        String invertida = "";
        for (int i = u.length() - 1; i >= 0; i--) {
            invertida += u.charAt(i);
        }
        return invertida;
    }

    public static void añadirPrefijo(String u, String v) {
        String prefijo = v;
        if (u == null && prefijo == null) {
            System.out.println("vacio");
        } else {
            System.out.println(u + prefijo);
        }
        return;

    }

    public static void añadirSufijo(String u, String v) {
        String sufijo = v;
        if (u == null && sufijo == null) {
            System.out.println("vacio");
        } else {
            System.out.println(sufijo + u);
        }
        return;
    }
}
